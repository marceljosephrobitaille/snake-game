import sqlite3
import tkinter as tk

from router import Router
from snake_game import SnakeGame
from pages import UsernamePage, StartPage, ScorePage, GamePage, GameoverPage


class App(tk.Tk):
    """
    This is the main class of the app (check main.py)
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.geometry(f"{SnakeGame.WIDTH_PX}x{SnakeGame.HEIGHT_PX}")
        self.resizable(0, 0)

        # Create router and add pages
        self.router = Router()
        self.router.add_page(StartPage(self))
        self.router.add_page(UsernamePage(self))
        self.router.add_page(ScorePage(self))
        self.router.add_page(GamePage(self))
        self.router.add_page(GameoverPage(self))
        # Switch to the default page
        self.router.switch_to_page('start')

    # TODO: We still have to figure out this
    # Maybe we should add some callback method to this class that sets a
    # username attribute and switches to the next page. The `UsernamePage` will
    # call this callback when the user fills the form or clicks a rencent user
    # button
    def create_user(self):
        new_user = self.username_field.get()
        conn = sqlite3.connect("SnakeGame_database.db")
        c = conn.cursor()
        print(new_user[0])
        c.execute("insert into user_score (username) values(?) ;", ("gatto"))
        conn.commit()
        # conn.close()
