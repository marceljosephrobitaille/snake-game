import math


class Vector:
    def __init__(self, x, y):
        self._x = x
        self._y = y

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    def __add__(self, other):
        return Vector(x=self._x + other._x, y=self._y + other._y)

    def __sub__(self, other):
        return Vector(x=self._x - other._x, y=self._y - other._y)

    def __neg__(self):
        return Vector(x=-self.x, y=-self.y)

    def __iter__(self):
        yield from (self._x, self._y)

    def __str__(self):
        return f'<Vector object: x={self._x} y={self._y}>'

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self._x == other._x and self._y == other._y

    def __hash__(self):
        return hash((self._x, self._y))

    def atan(self):
        return math.atan2(-self.y, self.x)


Vector.UP = Vector(x=0, y=-1)
Vector.DOWN = Vector(x=0, y=1)
Vector.RIGHT = Vector(x=1, y=0)
Vector.LEFT = Vector(x=-1, y=0)
Vector.DIRECTIONS = \
    frozenset((Vector.DOWN, Vector.RIGHT, Vector.UP, Vector.LEFT))
