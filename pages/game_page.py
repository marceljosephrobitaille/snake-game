from .page import Page
from snake_game import SnakeGame


class GamePage(Page):
    NAME = 'game'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.game = SnakeGame(self)
        self.username = None

    def on_show(self, username):
        self.username = username
        self.game.start_new_game()
