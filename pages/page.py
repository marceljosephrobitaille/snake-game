import tkinter as tk


class Page(tk.Frame):
    """
    This class represents a page of the app
    """

    # Must be implemented in subclass
    NAME = NotImplemented

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Set when page is added
        self.router = None

    def switch_to_page(self, name, *args, **kwargs):

        # Use router attribute to switch to another page
        self.router.switch_to_page(name=name, *args, **kwargs)

    def on_show(self):
        """
        Callback when a page is brought to the top

        To be overridden in subclass
        """
