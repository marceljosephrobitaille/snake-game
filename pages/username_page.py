import tkinter as tk
import sqlite3

from .page import Page
from new_user_form import NewUserForm


class UsernamePage(Page):
    """
    Username page

    To hold the new user form and the recent users list
    """

    NAME = 'username'

    NUM_USERNAMES = 10

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        user_field = NewUserForm(self, command=self.handle_set_username)
        user_field.pack()

        frame = tk.Frame(self)
        frame.pack()

        self.user_buttons = [
            tk.Button(frame)
            for _ in range(self.NUM_USERNAMES)
        ]
        for button in self.user_buttons:
            button.pack()

    def on_show(self):
        conn = sqlite3.connect("SnakeGame_database.db")
        c = conn.cursor()
        c.execute(
            "SELECT username FROM user_score GROUP BY username LIMIT ?;",
            (self.NUM_USERNAMES,),
        )

        for i, button in enumerate(self.user_buttons):
            username = c.fetchone()[0]
            button.configure(text=username, command=self.on_click)

        conn.close()

    def on_click(self):
        username = self.user_buttons[0].cget('text')
        print(username)

        if username:
            # self.command(username)
            self.switch_to_page('game', username=username)

    def handle_set_username(self, username):
        self.switch_to_page('game', username=username)
