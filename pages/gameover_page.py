import tkinter as tk

from .page import Page


class GameoverPage(Page):
    """
    Gameover Page

    Shown when the snake eats itself or crashes against a wall
    """

    NAME = 'gameover'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        label_lost = tk.Label(
            self,
            text='Fuck you looser \n you died hohoho',
        )
        label_lost.pack()

        label_score = tk.Label(
            self,
            text='Score: ----',
        )
        label_score.pack()

        play_again_button = tk.Button(
            self,
            text='Play again',
            command=lambda: self.switch_to_page('game'),
        )
        play_again_button.pack()

        menu_button = tk.Button(
            self,
            text='Back to main menu',
            command=lambda: self.switch_to_page('start'),
        )
        menu_button.pack()
