import sqlite3
import tkinter as tk

from .page import Page


class ScorePage(Page):
    """
    Score page

    To show the high scores
    """

    NAME = 'highscores'

    NUM_HIGHSCORES = 10

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        highscore_label = tk.Label(self, text='Highscores')
        highscore_label.pack()

        button_back = tk.Button(
            self,
            text="Back",
            command=lambda: self.switch_to_page('start'),
        )
        button_back.pack()

        frame = tk.Frame(self)
        frame.pack()

        self.labels = [tk.Label(frame) for _ in range(self.NUM_HIGHSCORES)]
        for label in self.labels:
            label.pack()

    def on_show(self):
        conn = sqlite3.connect("SnakeGame_database.db")
        c = conn.cursor()
        c.execute("""
            SELECT username, score
            FROM user_score
            ORDER BY score DESC
            LIMIT ?;
        """, (self.NUM_HIGHSCORES,))

        for i, label in enumerate(self.labels):
            username, score = c.fetchone()
            label.configure(text=f"{i+1}\t{username}\t{score}")

        conn.close()
