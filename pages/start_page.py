import tkinter as tk

from .page import Page


class StartPage(Page):
    """
    Start page class

    Holds the button to start a game and to show the highscores
    """

    NAME = 'start'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        button_start = tk.Button(
            self,
            text="Start a new game",
            command=lambda: self.switch_to_page('username'),
        )
        button_start.pack()

        button_scores = tk.Button(
            self,
            text="See highest score",
            command=lambda: self.switch_to_page('highscores'),
        )
        button_scores.pack()
