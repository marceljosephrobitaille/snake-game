class Router:
    """
    This class is responsible for storing all the pages and switching which one
    is on top at which time.
    """

    def __init__(self):
        self.pages = {}

    def add_page(self, page):
        """
        Add page to the list (dict, really) of known pages
        Also, place it absolutely in the window
        """

        self.pages[page.NAME] = page
        page.router = self

        # `place`, rather than `pack`, places the component relative to the
        # window rather than around its siblings
        # It's like creating a new layer on top of existing layers
        page.place(x=0, y=0, relwidth=1, relheight=1)

    def switch_to_page(self, name, *args, **kwargs):
        """
        Switch which page is on top

        Also call its callback, in case it needs to be notified of this change
        """

        page = self.pages[name]

        page.lift()

        page.on_show(*args, **kwargs)
