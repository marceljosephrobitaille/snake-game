import math
import random
import collections
from os import path
import tkinter as tk

from vector import Vector
from lib import sigmoid
from spritesheet import SpriteSheet
from resizing_canvas import ResizingCanvas


def relative_path(filename):
    return path.join(path.dirname(__file__), filename)


class SnakeGame:
    WIDTH = 24
    HEIGHT = 32
    TILE_SIZE = 16
    WIDTH_PX = WIDTH * TILE_SIZE
    HEIGHT_PX = HEIGHT * TILE_SIZE

    def __init__(self, page):

        self.page = page

        self.canvas = ResizingCanvas(
            page,
            width=self.WIDTH_PX,
            height=self.HEIGHT_PX,
            borderwidth=0,
            highlightthickness=0,
            bgimage='menu_screen_background.png',
        )
        self.canvas.pack()
        self.after_id = None

        self.score_label = self.canvas.create_text(
            self.WIDTH_PX,
            self.HEIGHT_PX,
            anchor=tk.SE,
            fill='white',
            font=('Arial', 15, 'bold'),
        )

        self.fruit_score = collections.OrderedDict([
            ('apple', 8),
            ('watermelon', 20),
            ('banana', 15),
            ('orange', 6),
            ('cherry', 2),
            ('pineapple', 17),
            ('grapes', 22),
            ('lemon', 1),
            ('strawberry', 3),
            ('blueberries', 4),
            ('pear', 7),
            ('coconut', 11),
        ])

        def generate_snake_sprites():
            sheet = SpriteSheet(
                imagepath=relative_path('./snake_sprites.png'),
                width=self.TILE_SIZE,
                height=self.TILE_SIZE,
            )
            sheet.define_range(
                names=['head', 'body-straight', 'body-curved', 'tail'],
            )

            return sheet

        def generate_fruit_sprites():
            sheet = SpriteSheet(
                imagepath=relative_path('./fruit_16pix.png'),
                width=self.TILE_SIZE,
                height=self.TILE_SIZE,
            )

            sheet.define_range(names=self.fruit_score.keys())

            return sheet

        self.spritesheets = {
            'snake': generate_snake_sprites(),
            'fruit': generate_fruit_sprites(),
        }

        # Generate Tk sprites for all sheets
        self.update_sprites()

    @property
    def head(self):
        return self.snake[-1]

    @property
    def tail(self):
        return self.snake[0]

    def start_new_game(self):
        self.score = 0

        self.snake = [Vector(0, 0), Vector(0, 1)]
        self.direction = Vector.DOWN

        self.place_random_fruit()

        self.update_speed_with_score()

        self.redraw()

        self.canvas.winfo_toplevel().bind('<KeyPress>', self.handle_key)
        self.after_id = self.canvas.after(int(self.period), self.update)

    def pause(self):
        self.canvas.after_cancel(self.after_id)
        # TODO

    def resume(self):
        self.redraw()
        self.after_id = self.canvas.after(int(self.period), self.update)

    def handle_gameover(self):
        self.page.switch_to_page('gameover')

    def update_sprites(self, event=None):
        for spritesheet in self.spritesheets.values():
            spritesheet.generate_tk_sprites(scale=self.canvas.current_scale)

    def place_random_fruit(self):
        while True:
            self.fruit_position = Vector(
                x=random.randrange(0, self.WIDTH),
                y=random.randrange(0, self.HEIGHT),
            )
            self.fruit_type = self.spritesheets['fruit'].random_sprite_name()

            if self.fruit_position not in self.snake:
                break

    def draw_sprite(self, vector, sprite, tags):
        center = Vector(
            x=vector.x * self.TILE_SIZE,
            y=vector.y * self.TILE_SIZE,
        )

        self.canvas.create_image(
            center.x, center.y,
            image=sprite,
            anchor=tk.NW,
            tags=tags,
        )

    def draw_snake(self):
        self.canvas.delete('snake')

        for i in range(1, len(self.snake) - 1):
            prev_v = self.snake[i - 1]
            curr_v = self.snake[i]
            next_v = self.snake[i + 1]

            prev_d = curr_v - prev_v
            next_d = next_v - curr_v

            def normalize(angle):
                return math.atan2(math.sin(angle), math.cos(angle))

            if prev_d == next_d:
                self.draw_sprite(
                    vector=curr_v,
                    sprite=self.spritesheets['snake'].get(
                        'body-straight',
                        direction=next_d,
                    ),
                    tags='snake',
                )

            else:
                prev_a = prev_d.atan()
                next_a = next_d.atan()

                mirror = normalize(prev_a - next_a) > 0
                direction = -next_d if mirror and next_d.x == 0 else next_d

                self.draw_sprite(
                    vector=curr_v,
                    sprite=self.spritesheets['snake'].get(
                        'body-curved',
                        direction=direction,
                        mirror=mirror,
                    ),
                    tags='snake',
                )

        self.draw_sprite(
            vector=self.tail,
            sprite=self.spritesheets['snake'].get(
                'tail',
                direction=self.snake[1] - self.snake[0],
            ),
            tags='snake',
        )

        self.draw_sprite(
            vector=self.head,
            sprite=self.spritesheets['snake'].get(
                'head',
                direction=self.direction,
            ),
            tags='snake',
        )

    def redraw(self):

        self.draw_snake()

        self.canvas.delete('fruit')
        self.draw_sprite(
            self.fruit_position,
            self.spritesheets['fruit'].get(self.fruit_type),
            tags='fruit',
        )

        self.canvas.itemconfig(self.score_label, text=f'Score: {self.score}')

    def handle_key(self, event):
        actual_direction = self.snake[-2] - self.snake[-1]

        if event.keysym in ('Up', 'w') and \
                actual_direction != Vector.UP:
            self.direction = Vector.UP

        elif event.keysym in ('Down', 's') and \
                actual_direction != Vector.DOWN:
            self.direction = Vector.DOWN

        elif event.keysym in ('Right', 'd') and \
                actual_direction != Vector.RIGHT:
            self.direction = Vector.RIGHT

        elif event.keysym in ('Left', 'a') and \
                actual_direction != Vector.LEFT:
            self.direction = Vector.LEFT

        elif event.keysym == 'space':
            self.pause()

    def check_loose_condition(self, head):
        if head in self.snake:
            return True

        if head.x < 0 or head.y < 0 or \
                head.x >= self.WIDTH or head.y >= self.HEIGHT:
            return True

        return False

    def update_speed_with_score(self):
        INITIAL_PERIOD = 250
        MIN_PERIOD = 80

        # Score where snake is going super fast
        FASTEST_SCORE = 400

        self.period = (INITIAL_PERIOD - MIN_PERIOD) * \
            sigmoid(6 * (1 - 2 * self.score / FASTEST_SCORE)) + MIN_PERIOD

    def update(self):

        # Add new head in direction of snake
        new_head = self.head + self.direction

        if self.check_loose_condition(new_head):
            self.handle_gameover()
            return

        self.snake.append(new_head)

        if self.head == self.fruit_position:
            self.score += self.fruit_score[self.fruit_type]
            self.place_random_fruit()

        else:
            # Remove tail
            self.snake.pop(0)

        self.redraw()

        self.update_speed_with_score()
        self.after_id = self.canvas.after(int(self.period), self.update)
