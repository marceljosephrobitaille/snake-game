import math
import random
import itertools
from vector import Vector
from PIL import Image, ImageTk


class SpriteSheet:
    def __init__(self, imagepath, width, height):
        self._spritesheet = Image.open(imagepath)
        self._pil_sprites = {}
        self._tk_sprites = {}

        self._default_sprite_width = width
        self._default_sprite_height = height

    def define(self, name, x, y, width=None, height=None):
        width = width or self._default_sprite_width
        height = height or self._default_sprite_height

        self._pil_sprites[name] = \
            self._spritesheet.crop((x, y, x + width, y + height))

    def define_range(self, names, width=None, height=None, start_x=0):
        width = width or self._default_sprite_width
        height = height or self._default_sprite_height

        for i, name in enumerate(names):
            tot_x = start_x + i * width
            x = tot_x % self._spritesheet.width
            y = (tot_x // self._spritesheet.width) * height

            self.define(name=name, x=x, y=y, width=width, height=height)

    def generate_tk_sprites(self, scale):
        def generate_sprite(orig_sprite, direction, mirror):
            sprite = orig_sprite \
                .rotate(math.degrees(math.atan2(-direction.y, direction.x))) \
                .resize((
                    int(orig_sprite.width * scale),
                    int(orig_sprite.height * scale),
                ), Image.NEAREST)

            if mirror:
                sprite = sprite.transpose(Image.FLIP_TOP_BOTTOM)

            return ImageTk.PhotoImage(sprite)

        for name, orig_sprite in self._pil_sprites.items():
            self._tk_sprites[name] = {
                (direction, mirror): generate_sprite(
                    orig_sprite,
                    direction=direction,
                    mirror=mirror,
                )
                for direction, mirror in itertools.product(
                    Vector.DIRECTIONS,
                    (False, True),
                )
            }

    def get(self, name, direction=Vector.RIGHT, mirror=False):
        return self._tk_sprites[name][(direction, mirror)]

    def random_sprite_name(self):
        return random.choice(list(self._pil_sprites.keys()))
