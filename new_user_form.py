import tkinter as tk


class NewUserForm(tk.Frame):
    def __init__(self, master, command, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self.command = command

        label = tk.Label(self, text="Create a new username:")
        label.pack()

        self.text_field = tk.Entry(self)
        self.text_field.pack()

        button = tk.Button(self, text="Play!", command=self.on_click)
        button.pack()

    def on_click(self):
        username = self.text_field.get()

        if username:
            self.command(username)
